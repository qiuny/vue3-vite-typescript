import { createApp } from 'vue';
import App from './App.vue';
import { store, key } from './store';
// 注入路由
import router from './router';

// 注入ui组件库
import ElementPlus,{ElNotification} from 'element-plus';
import 'element-plus/dist/index.css';

import 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'

// 引入font-awesome
import '@/assets/font-awesome/css/font-awesome.css';

import * as ElIcons from '@element-plus/icons-vue';

import { start, close} from "@/script/utils/nprogress";

import AuthorInfo from '@/components/author-info/index.vue';
import ArticleLabel from '@/components/article-label/index.vue';
import WebSiteInfo from '@/components/website-info/index.vue';

// 当前时间格式
import {currentFormat} from '@/script/utils';

const app = createApp(App);

// 批量注入icon图标组件
Object.keys(ElIcons).forEach(key =>{
    app.component( key, ElIcons[key]);
})

app.component('AuthorInfo', AuthorInfo)
app.component('ArticleLabel', ArticleLabel)
app.component('WebSiteInfo', WebSiteInfo)

app.use(ElementPlus);
app.use(VXETable);

app.use(store, key);
app.use(router);
app.mount('#app');

// 挂载方法
app.config.globalProperties.$currentFormat = currentFormat;

// 设置路哟导航
router.beforeEach((to, from, next)=>{
    start();
    // console.log(to, from, next,111);
    if(to.meta.title){
        document.title = String(to.meta.title)
    }else document.title = '布衣博客'

    // 路由出错处理
    if(to.matched.length===0&&!to.name){
        ElNotification({
            title: '错误提示！',
            message: `【${to.fullPath}】路径找不到对应页面，默认切换到首页...`,
            type: 'error',
            duration: 3000
        })
        router.push('/home')
    }
    next()
})

// 路由加载结束后执行
router.afterEach(()=>{
    close()
})