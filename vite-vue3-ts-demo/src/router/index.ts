import { createRouter, createWebHashHistory, RouteRecordRaw} from "vue-router";
import LayOut from "@/components/layout/index.vue";

const routes:Array<RouteRecordRaw>=[
    {
        path: '/',
        component: LayOut,
        redirect: '/home',
        hidden: false,
        children:[
            {
                path: '/home',
                name: 'home',
                hidden: false,
                component: ()=> import("@/pages/home/index.vue"),
                meta:{
                    title: '首页',
                    icon: '',
                    index: '1'
                }
            },
            {
                path: '/program',
                name: 'program',
                hidden: false,
                component: ()=> import("@/pages/program/index.vue"),
                meta:{
                    title: '编程知识',
                    icon: '',
                    index: '2'
                },
                children:[
                    {
                        path: '/program/front',
                        name: 'program-front',
                        component: ()=> import("@/pages/program/front/index.vue"),
                        meta:{
                            title: '前端',
                            icon: '',
                            index: '2-1'
                        }
                    },
                    {
                        path: '/program/behind',
                        name: 'program-behind',
                        component: ()=> import("@/pages/program/behind/index.vue"),
                        meta:{
                            title: '后端',
                            icon: '',
                            index: '2-2'
                        }
                    }
                ]
            },
            {
                path: '/about',
                name: 'about',
                component: ()=> import("@/pages/about/index.vue"),
                meta:{
                    title: '关于',
                    icon: '',
                    index: '3'
                }
            }
        ]
    },
    {
        path: "/article",
        name: "article",
        component: LayOut,
        redirect: "/article/info",
        meta:{
            title: '文章',
            index: ''
        },
        children:[
            {
                path: "/article/info",
                name: "article-info",
                hidden: false,
                meta:{
                    title: '文章详情',
                    index: '1'
                },
                component:()=> import("@/pages/common/article/info.vue")
            }
        ]    
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    // routes = routes:routes
    routes
})

export default router;
