// 全局挂载文件读取方法
import { ComponentInternalInstance, getCurrentInstance} from "vue";

export default function globalCurrentInstance(){
    const { appContext } = getCurrentInstance() as ComponentInternalInstance
    const globalConfig = appContext.config.globalProperties
    return{
        globalConfig   
    }
}