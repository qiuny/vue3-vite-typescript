# vite-vue3+ts+volar

#### 介绍
2021年最新完整Vite+Vue3+TypeScript项目实战；
随着前端web应用的需求不断发展和变化，vue生态圈也紧跟开发者步伐，不断演化。尽管vue2.0已经很完善了，很多人掌握的vue2.0，感觉实在学不动了，意料之外的是尤先生继续更新vue到3.0版本，以补充vue2.0的不足之处

#### 软件架构
软件架构说明


#### 安装教程

1、克隆项目；

```bash
https://gitee.com/qiuaiyun/vite-vue3-ts-volar.git
```

2、安装依赖；

```bash
npm install
```

#### 使用说明
1、启动项目

```bash
npm run dev
```

2、 打包项目

```bash
npm run build
```

3、预览项目

```bash
npm run serve
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技  

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
